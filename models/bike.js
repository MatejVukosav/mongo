/**
 * Created by mvukosav on 11.6.2016..
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var bikeSchema = new Schema({
    bike_id: Number,
    owner: String,
    time: Float32Array,
    lock: ObjectId,

   // autoIndex: false
});
bikeSchema.statics.findById = function (bike_id, cb) {
    return this.find({ bike_id: new RegExp(bike_id, 'i') }, cb);
}

var Bike=mongoose.model('Bike',bikeSchema);