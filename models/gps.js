/**
 * Created by mvukosav on 11.6.2016..
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gpsSchema = new Schema({
    coordinateX: Float32Array,
    coordinateY: Float32Array,
    coordinateZ: Float32Array
});

var GPS=mongoose.model('GPS',gpsSchema);